package com.example.androidb11new;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText textField;
    Button remove;
    Button restore;
    List<String> words = new ArrayList<>();
    int countOfWords;
    int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textField = findViewById(R.id.textToSave);
        remove = findViewById(R.id.remove);
        restore = findViewById(R.id.restore);

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!textField.getText().toString().equalsIgnoreCase("")){
                    words.add(textField.getText().toString());
                }
                textField.setText("");
            }
        });

        restore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index += 1;
                countOfWords = words.size() - index;
                if (countOfWords >= 0){
                    textField.setText(String.valueOf(words.get(countOfWords)));
                }
            }
        });
    }
}
